from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from xlrd import XLRDError
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from threading import Thread
import os.path
import pandas
import time
import math
import autoit

'''
    Global variables that needs to be shared between functions,
    maybe it is possible to not use some of them, but anyway
    it is faster.
'''
global filename
global medianame
global progress
global numbers
global cella
global foglio
global root
global status
global excel_data
global status_label
global stop
global pause
global resume
global stop_button
global resume_button
global pause_button
global sending
'''
    Definition of variables
'''
sending = False
stop = False
pause = False
numbers = 2
root = Tk()
mainframe = ttk.Frame(root, padding="10 10 12 12")
cella = StringVar()
filename = StringVar()
medianame = StringVar()
foglio = StringVar()
foglio_entry = ttk.Entry(mainframe,textvariable=foglio)
message_entry = Text(mainframe,width=40,height=10)
cella_entry = ttk.Entry(mainframe,textvariable=cella)
status = StringVar()
status_label =  ttk.Label(mainframe, textvariable=status)
status_label.grid(column=1, row=7, sticky=(W,E,S,N))
driver = webdriver.Chrome()

'''
    The function that sends messages
'''

def send():
    global sending
    sending = True
    '''
        Reading the excel file and in order:
        > Get the total numbers to send
        > Creating a correctly dimensioned progress bar
        > Placing the progress bar
        Possible exceptions:
        > FileNoutFoundError : when file is not selected
        > XLRDError : when sheet name is missing
        > KeyError : when the column name is missing
    '''
    global status_label
    global driver
    global wait
    global stop
    global pause
    driver.get("https://web.whatsapp.com/")
    wait = WebDriverWait(driver, 1000)
    try:
        excel_data = pandas.read_excel(str(filename.get()), sheet_name=str(foglio.get()))
        numbers = len(excel_data[str(cella.get())].tolist())
        progress = ttk.Progressbar(root,maximum=numbers, orient=HORIZONTAL, length=200, mode='determinate')
        progress.grid(column=0,row=5,sticky=(W,E))
        '''
            For each row of the given column
            if the row is not empty then, do things
            else do nothing and just step the bar
        '''
        '''
            Put here anything that requires to be defined
        '''

        for column in excel_data[str(cella.get())].tolist():
            '''
                Checking for stopped or paused
            '''
            if (stop is True):
                status_label['foreground'] = "red"
                status.set("Fermato a:" + str(column))
                break
            if (pause is True):
                while pause:
                    status_label['foreground'] = "blue"
                    status.set("Pausa")
                    time.sleep(1)
                if (stop is True):
                    status_label['foreground'] = "red"
                    status.set("Fermato a:" + str(column))
                    break

            if (math.isnan(column) is False ):
                status_label['foreground'] = "black"
                status.set("Invio messaggio a: " + str(int(column)))
                progress.step()
                time.sleep(1)
                '''
                    Put here all the instructions to perform with the number
                '''
                ''' Uncomment to test when you have chromedriver.exe in the same dir
                '''
                
                search_box = '//*[@id="side"]/div[1]/div/label/div/div[2]'
                person_title = wait.until(lambda driver:driver.find_element_by_xpath(search_box))
                person_title.clear()
                # Send contact number in search box
                person_title.send_keys(str(int(column)))
                # Wait for 2 seconds to search contact number
                time.sleep(2)
                try:
                  # Load error message in case unavailability of contact number
                  element = driver.find_element_by_xpath('//*[@id="pane-side"]/div[1]/div/span')
                except NoSuchElementException:
                  person_title.send_keys(Keys.ENTER)
                  actions = ActionChains(driver)
                  actions.send_keys(str(message_entry.get(1.0,"end")))
                  actions.send_keys(Keys.ENTER)
                  actions.perform()
                  if file_exists(str(medianame.get())) is True: #check if media file exist, and then send it
                    send_attachment()
                  
            else:
                '''
                    Put here all the instructions to perform when we find
                    an empty value on the list
                '''
                progress.step()
                time.sleep(1)
            root.update()
        stop = False
        time.sleep(5)
        status_label['foreground'] = "green"
        status.set("Completato")
        sending = False
    except FileNotFoundError:
        status_label['foreground'] = "red"
        status.set("Errore! File non trovato!")
    except XLRDError:
        status_label['foreground'] = "red"
        status.set("Errore! Nome del foglio mancante o errato!")
    except KeyError:
        status_label['foreground'] = "red"
        status.set("Errore! Nome della colonna mancante o errata!")


'''
    Callback that opens a dialog and lets the user choose a file
'''
def seleziona_file():
    filename.set(filedialog.askopenfilename())

def seleziona_media():
    medianame.set(filedialog.askopenfilename())

'''
    Simple callback to update the StringVar variable
'''
def callback(sv):
    sv = sv.get()

def stop_process():
    global sending
    if (sending is False):
        return
    global stop
    stop = True
    global status
    status.set("Sto fermando il processo, attendere il completamento della seguente operazione...")
def pause_process():
    global sending
    if (sending is False):
        return
    global pause
    pause = True
    global status
    status.set("Sto mettendo in pausa il processo, attendere il completamento della seguente operazione...")
    global resume_button
    global stop_button
    stop_button.grid_forget()
    resume_button.grid(column=2, row=6, sticky=W)
def resume_process():
    global sending
    if (sending is False):
        return
    global pause
    pause = False
    global status
    global stop_button
    global resume_button
    status.set("Sto riprendendo l'esecuzione, attendere...")
    resume_button.grid_forget()
    stop_button.grid(column=2, row=6, sticky=W)

def start_sender_thread():
    thread = Thread(target = send)
    thread.start()
'''
	Starting to work on media sending
'''
'''
    Return true if the given path to file exists
'''
def file_exists(path_to_file):
        return os.path.isfile(path_to_file)
'''
    TODO : test it
'''
def send_attachment():
    # Attachment Drop Down Menu
    global driver
    try:
        clipButton = driver.find_element_by_xpath('//*[@id="main"]/footer/div[1]/div[1]/div[2]/div/div/span')
        clipButton.click()
    except:
        traceback.print_exc()
    time.sleep(1)

    # To send Videos and Images.
    try:
        mediaButton = driver.find_element_by_xpath('//*[@id="main"]/footer/div[1]/div[1]/div[2]/div/span/div/div/ul/li[1]/button')
        mediaButton.click()
    except:
        traceback.print_exc()
    time.sleep(10)
    autoit.control_focus("Apri", "Edit1")
    autoit.control_set_text("Apri", "Edit1", os.path.normcase(str(medianame.get())))
    autoit.control_click("Apri", "Button1")

    time.sleep(3)
    # Send button
    try:
        whatsapp_send_button = driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[2]/span/div/span/div/div/div[2]/span/div/div')
        whatsapp_send_button.click()
    except:
        traceback.print_exc()
    
'''
    Widgets setup
'''
stop_button = ttk.Button(mainframe, text="Stop",command=stop_process )
pause_button = ttk.Button(mainframe, text="Pausa",command=pause_process )
resume_button = ttk.Button(mainframe, text="Riprendi",command=resume_process )
root.title("Invio automatico di messaggi whatsapp")
filename.set('Nessun file selezionato')
medianame.set('Nessun file selezionato')

mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
'''
    In order to resize the text message, we add the two
    lines below
'''
mainframe.columnconfigure(1,weight=1)
mainframe.rowconfigure(0, weight=1)


'''
    Message entry for typing the message to send
'''

ttk.Label(mainframe, text="Messaggio").grid(column=0, row=0, sticky=(W))
message_entry.grid(column=1, row=0, sticky=(W,E,N,S))
'''
    For sending photo/videos/gifs
'''
ttk.Label(mainframe, text="Media :").grid(column=0, row=2, sticky=W)
ttk.Label(mainframe, textvariable=medianame).grid(column=1, row=2, sticky=W)
ttk.Button(mainframe, text="Seleziona file",command=seleziona_media ).grid(column=2, row=2, sticky=W)
'''
    Filename is for selecting the file
'''
ttk.Label(mainframe, text="File Excel :").grid(column=0, row=3, sticky=W)
ttk.Label(mainframe, textvariable=filename).grid(column=1, row=3, sticky=W)
ttk.Button(mainframe, text="Seleziona file",command=seleziona_file ).grid(column=2, row=3, sticky=W)
'''
    Foglio entry is for typing the sheet where to find the infos
'''
ttk.Label(mainframe, text="Foglio :").grid(column=0, row=4, sticky=W)
foglio.trace("w", lambda name, index, mode, foglio=foglio: callback(foglio))
foglio_entry.grid(column=1, row=4, sticky=(W, E))

'''
    Cella is for chosing the name of the column where to find the numbers
'''
ttk.Label(mainframe, text="Nome elenco :").grid(column=0, row=5, sticky=W)
cella.trace("w", lambda name, index, mode, cella=cella: callback(cella))
cella_entry.grid(column=1, row=5, sticky=(W, E))

'''
    Positioning all the widgets
'''
ttk.Label(mainframe, text="Status :").grid(column=0, row=7, sticky=W)
ttk.Button(mainframe, text="Invia messaggio",command=start_sender_thread ).grid(column=1, row=6, sticky=W)
pause_button.grid(column=3, row=6, sticky=W)
stop_button.grid(column=2, row=6, sticky=W)
'''
    Main loop
'''
root.mainloop()
